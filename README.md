# Computadora II
## 1.- Mapa Conceptual-Computadoras Electrónicas


```plantuml
@startmindmap
<style>
rootNode {
  Padding 12
    Margin 3
    HorizontalAlignment center
    LineColor red
    LineThickness 3.0
    BackgroundColor pink
    RoundCorner 40
    MaximumWidth 100
    }
</style>
* **Computadoras Electrónicas**
** **Inicios del siglo XX**
*** Las computadoras se hicieron muy presentes en el gobierno y negocios.
**** Aumentó la necesidad del uso de las computadoras por lo que se necesitaba una innovación urgente.
*** Ayudaron a realizar mucha tareas manuales y a algunas las remplazaban por completo.
*** Surge el aumento de las necesidades de automatización y de computación.
** **Computadora Harvard Mark I**
*** Construida por IBM en 1944.
**** Se construyó durante la Segunda guerra mundial para los aliados.
*** Características
**** Se componía por 765,000 componentes.
**** 80 kilómetros de cable.
**** Eje de 15 metros que atravesaba por completo la estructura y así mantenerla en sincronía.
**** Motor de 5HP. 
**** Tenía aproximadamente 3500 Relés.
*** Usos que se le dieron.
**** Hacer simulaciones y cálculos para el proyecto Manhattan.
***** El proyecto para el cual se desarrolló la bomba atómica.
*** Piedra angular de la computadora.
**** Era de Relé
***** Es un interruptor mecánico controlado eléctricamente.
****** Compuesto por:
******* Bobina
******* Brazo de hierro móvil
******* Dos contactos
****** Funcionamiento
******* 1.- La corriente eléctrica atraviesa la bobina generando un campo magnético.
******** Los electrones viajan a través de la bobina que no es más que un alambre de cobre aislado y enrollado.
******* 2.- El campo magnético generado atraé al brazo metálico mpovil.
******* 3.- El brazo métálico logra unir a los dos contactos cerrando el circuito y haciendo que funcione.
****** El circuito controlado puede estar conectado a:
******* Otro circuito
******* Un motor eléctrico
******** Este motor podría tener un engranaje representando una suma.
****** Inconvenientes
******* El brazo de hierro de los Relés que se encarga de conectar el circuito tiene masa, por consiguiente entra la innercia.
******** Innercia: oposición que los cuerpos presentan al cambiar su estado en movimiento.
********* El cambio de estado abierto o cerrado del circuito toma un cierto tiempo.
********* En 1950 un buen Relé cambiaba de estados 50 veces por segundo.
********* Se tenían que trabajar con problemas grandes y complejos.
******* Vida útil
******** Debido a sus partes móviles era más susceptible sufrir daños por uso y por tiempo. 
******* A mayor cantidad de Relés mayores  son los inconvenientes y daños que puede sufrir. 
******** Estadísticamente fallaba 1 relé al día.
******* Al ser máquinas calientes y oscuras atraían bichos e insectos.
******* En 1947 se encontró una polilla en uno de los Relés de la máquina.
******** Grace Hopper, científica de computación.
********* Nace el término "Bugs" para los errores de las computadoras.
**** Podía realizar:
***** Tres sumas o restas por segundo.
***** Multiplicación en 6 segundos.
***** División en 15 segundos.
***** Las operaciones complejas las hacía en minutos.
** **Válvula termoiónica**
*** Desarrollada en 1904
**** Por el físico inglés John Ambrose Fleming.
*** Formada por: 
**** Un filamento
**** Dos electrodos situados dentro de un Bulbo de cristal llamados:
***** Ánodo
***** Cátodo
*** Proceso de emisión termoiónica:
**** 1.- Al encenderse o calentarse el filamento, perimite el flujo de electrónes desde el ánodo hasta el cátodo.
*** Permite el flujo de la corriente en una sola dirección. 
*** Tubos triodos
**** En la actualidad se conoce como diodo.
**** Lee De Forest
***** Inventor Americano
***** En el año 1906 
****** Agregó el electrodo de control a la válvula termoiónica.
****** Se encontraba entre el ánodo y el cátodo del diseñpo de físico Fleming.
****** Función: 
******* Cuando se aplicaba una carga positiva o negativa en el electrodo de control
******** Permite el flujo o detiene la corriente.
******** Con esto se logra el mismo funcionamiento de los relés.
****** Ventajas
******* Ya no existe el inconveniente de las partes móviles.
******* Es más confiable pues es menor el riesgo de que existan bugs.
******* Tenían mayor velocidad
******* Podían cambiar de estado miles de veces por segundo.
******* Inició la era de las computadoras electrónicas.
****** Fueron la base de distintos dispositivos:
******* La radio
******* Teléfonos de larga distancia
******* Muchos dispositivos electrónicos de los años 1950.
****** Desventajas
******* No eran perfectos
******* Eran frágiles
******* Podían quemarse como las bombillas o focos.
******* Eran de material costoso.
******* Las computadoras requerían cientos de miles de tubos de vació.
** **Colossus Mark I**
*** Diseñada por el Ingeniero Tommy Flowers
*** Finalizada en diciembre de 1943
*** Se instaló en Reino Unido, Bletchley Park.
*** Tubo el primer uso de tubos de vacío a gran escala.
*** Funciones:
**** Ayudó en la decodificación de las comunicaciones nazis.
***** Utilizaron 10 Colossus para la decodificación.
*** Nota: Alan Turing diseñó dos años antes el dispositivo The Bombe
**** Utilizado para decifrar el código enigma Nazi.
*** Características
**** Contenia 1600 tubos de vacío. 
**** Conocida como la primer computadora electrónica programable.
*** Programación de la computadora
**** Se hacía mediante la conexión de cientos de cables encontrados en un tablero.
***** Era similar a los tableros de conmutación de los teléfonos en esa época.
** **Computadora ENIAC**
*** Sus siglas significan:
**** Calculadora
**** Integradora
**** Numérica 
**** Electrónica
*** Se construyó en 1946 en la Universidad de Pensilvania.
*** Diseñada por:
**** John Mauchly
**** J. Presper Eckert
*** Fue la primera computadora programable de propósito general.
*** Características
**** Podía realizar cinco mil sumas y restas de 10 dígitos por segunto.
**** Podía hacer muchos más cálculos que cualquier dispositivo de esa época.
**** Su tiempo de vida fue de 10 años operando.
**** Realizó muchos más cálculos aritméticos que la humanidad en conjunto en esa época.
*** Inconvenientes
**** Debido a que operaba con tubos de vacío se presentaban constantes fallas de los mismos.
**** Por ello solo se operaba hasta la mitad del día.
**** En la década de 1950 la computación basada en tubos de vacío comenzaba a tener fallos y llegar a sus límites.
**** No fue posible reducir costes y tamaño, ni aumentar la rápidez ni confiabilidad con el uso de los tubos de vacío.
** **Invensión del transistor**
*** Inició una nueva era de la computación.
*** Se inventó en 1947 por los Laboratorios Bell.
**** Por los científicos 
***** John Bardeen
***** Wlater Brattain
***** William Shockely
*** Caracterpisticas de los transistores.
**** Cumple la misma función que un Relé y los tubos de vacío.
***** Lo cual es un interruptor eléctrico.
**** Hechos del elemento químico Silicio.
***** Nota: Este material en estado puro no es conductor eléctrico.
**** Con el paso Doping o Dopaje se le agregan al silicio los elementos como:
***** Arsénico, fósforo, aluminio, indio y galio.
****** Con estos elementos se permite la conducción de la corriente eléctrica.
****** Dependiendo del elemento que se le agregue al silicio puede tener:
******* Exceso o deficiencia de electrones.
**** Construido con tres capas de los tipos del silicio en forma de Sandwich.
***** Cada capa representa un conector:
****** Los conectores se llaman:
******* Colector
******* Emisor
******* Base
***** ¿Cómo funcionan?
****** Si existe un flujo de corriente entre la base y el emisor.
******* Tmbién permite que se de el flujo de corriente entre el emisor y el colector.
****** Así es como cumple como interruptor eléctrico.
**** El primer transistor era muy comprometedor.
***** Podría cambiar de estados 10,000 veces por segundo.
**** Los transistores eran más sólidos y  mucho más resistentes.
**** Podían hacerse muy pequeños y ocupaban menos espacio.
*** Ventajas
**** El uso de menos espacio ya que podían ser más pequelos que un Relé o los tubos de vacío.
**** Pudieron construirse computadoras mucho más baratas.
***** Un ejemplo fue la IBM 608 en 1957.
*** La primer computadora totalmente basada en transistores podía:
**** 4500 sumas y 80 divisiones y multiplicaciones por segundo.
*** Con esto IBM utilizó en todas sus computadoras a los transitores
**** Por ello las computadoras pudieron ingresarse en las oficio¿na sy en los hogares.
*** Actualmente las computadoras utilizan transistores con tamaños menores a 50 nanómetros.
**** Son transistores diminutos .
**** Cambian de estado millones de veces por segundo.
**** Pueden funcionar décadas.

@endmindmap
```
## 2.- Mapa conceptual Arquitectura Von Neumann y Arquitectura Harvard

```plantuml
@startmindmap

@startmindmap
<style>
rootNode {
  Padding 12
    Margin 3
    HorizontalAlignment center
    LineColor blue
    LineThickness 3.0
    BackgroundColor white
    RoundCorner 40
    MaximumWidth 100
    }
</style>
* **Arquitectura de Computadoras**
** **Ley de Moore**
*** Establece que:
**** "La velocidad del procesador o el poder de procesamiento total de las computadoras se duplica cada doce meses"
***** Prcesador
****** Internamente se compone por transistores.
******* Los transistores tienen combinaciones de compuertas lógicas.
******* Combinandas resolvían una operación en específico.
*** Electrónica
**** El número de transitores por chip se duplica cada año.
**** El costo del chip permanece sin cambios.
**** Se duplioca la potencia del cálculo sin modificar el costo cada 8 meses.
*** Performance 
**** Se incrementa la velocidad del procesador.
**** Se incrementa la capacidad de la memoria.
**** La velocidad de la memoria corre siempre por detrás de la velocidad del procesador.
** **Funcionamiento de una computadora** 
*** Nivel Funcional
**** Anteriormente la computadora utilizaba un sistema de cableado
***** Se enchufaba y se desenchufaba.
**** Un ejemplo fue la computadora o dispositivo de Alan Turing
**** La programación era a través del hardware.
***** Es decir cada que se realizaba una tarea se tenía que cambiar el hardware
**** Características:
***** Tenía una entrada de datos
***** Los cables elegían que función hacía mediante la secuencia de funciones aritméticas/lógicas.
***** Finalmente presentaba los resultados.
**** Sistema actual
***** En la actualidad la programación de la computadora es mediante software
****** En cada tarea o paso que se realiza se hace una operación sobre los datos.
****** Características:
******* 1.- Se ingresa la entrada de datos.
******* 2.- Existe una secuencia de funciones aritméticas/lógicas.
******* 3.- Le ordenamos lo que queremos que realice.
******* 4.- Tiene un intérprete de instrucciones.
******* 5.- Las señales de control le dicen al sistema cual de las funciones se van a ejecutar.
******* 6.- Finalmente arroja un resultado.
******* El sistema se realiza de forma iterativa.
** Arquitectura de Von Neumann
*** Arquitectura moderna
*** Esta arquitectura es de un diseño electronico de computadora con partes:
*** De esta arquitectura existen tres partes fundamentales:
**** La CPU o Unidad de Procesamiento
***** En esta unidad se encuentran los registros al momento de ejecución:
****** PC o controlador de programa
****** IR que es el registro de instrucción
****** MAR (Registro de dirección de memoria)
****** MBR (Registro de buffer de memoria)
****** E/S AR (Registro de dirección de entrada/salida)
****** E/S BR (Registro de buffer de ENTRADA/SALIDA)
***** ¿Cómo funciona?
****** Al ejecutar algo se ingresa por el módulo de E/S 
****** Los elementos se comunican a través del bus del sistema en la placa MAR.
****** El dato de la ejecución pasa a la memoria principal
****** Posteriormente a la memoria la información pasa a la CPU para que ésta la trabaje
**** La memoria principal 
**** Módulo de entrada/salida
*** Modelo 
**** Escrita en 1945 por el matemático y físico John Von Neaumann y otros más.
**** Primer borrador en un informe sobre EDVAC
***** Los datos y programas se almacenan en una misma memoria de Lectura-Escritura.
***** Se accede a los contenidos de la memoria indicando su ubicación o posición sin que importe el tipo de contenido.
***** La ejecución es de dorma secuelcial a menos que se indique lo contrario.
***** Su representación es de forma binaria.
***** El Bus de sistema se dividen en tres partes:
****** Bus de control
****** Bus de direcciones
****** Bus de datos e instrucciones
**** De este modelo surge el concepto de programa almacenado.
***** La computadoras son conocidas por este concepto.
**** Surgió un problema debido a la separación de la memoria y CPU
***** A este problema se le denominó Neumann Bottleneck o cuello de botella de Neumann.
****** A causa de: 
******* La cantidad de datos que pasa entre la memoria y el CPU difiere en tiempo con la velocidad de ambos.
******* Se denomina throughput.
******* Debido a esto la CPU se puede tornar ociosa.
*** Interconexión 
**** Todos los componentes se comunican a través del sistema de buses.
**** Modelo de Bus
***** ¿Qué es el bus?
****** Es un dispositivo en común entre dos o más dispositivos.
****** Existen varios tipos de buses que hacen la tarea de interconexión entre distintas partes de la computadora.
****** Transmisión de señales:
******* Cuando dos dispositivos transmiten al mismo tiempo señales, las señales pueden distorsionarse y perder información.
******* Debe haber un arbitraje para decidir qué dispositivo hace uso del bus.
******* Cada línea puede transmitir señales con representación de unos y ceros.
******** Solo se transmite una señal por unidad de tiempo y en forma secuencial.
** **Arquitectura Hardvard**
*** Arquitectura Microcontroladores
*** Modelo
**** Utilizaba dispositivos de almacenamiento físicamente separados para las instrucciones y para los datos.
**** Constituido por los elementos:
***** CPU (Unidad central de procesamiento) 
****** Compuesta por:
******* Unidad de control
******* Unidad aritmética/lógica
******* Registros
***** Memoria principal
****** Puede almacenar
****** Instrucciones
****** Datos
***** Sistema de entrada/salida.
*** Dificultades en la memoria.
**** El costo es alto para fabricar memorias más rápidas.
**** Solución:
***** Proporcionar la memoria CACHÉ 
****** Esta es una pequeña cantidad de memoria.
***** Mientras los datos que necesita el procesador estén en la memoria caché.
****** El rendimiento será mayor.
*** Solución Hardvard
*** Usos de la arquitectura Hardvard 
**** PICs
**** Microcontroladores
***** Usados en electrodomésticos 
***** Usado en procesamiento de audio y video.



@endmindmap
```



## 3.- Mapa conceptual Basura Electrónica


```plantuml
@startmindmap

@startmindmap
<style>
rootNode {
  Padding 12
    Margin 3
    HorizontalAlignment center
    LineColor green
    LineThickness 3.0
    BackgroundColor gray
    RoundCorner 40
    MaximumWidth 100
    }
</style>
* **Basura Electrónica**
** **Secretos de la basura electrónica**
*** Centro de recicleje e-end
**** Se almacena cientos de basura electrónicas diarias.
**** Se procesan:
***** Televisores
***** Computadoras
***** Celulares viejos
***** Cada parte de estos dispositivos son basura electrónica procesada
**** Ubicada en Estados Unidos, estado de Merryland.
**** Al desechar los aparatos puede volverse un peligro.
***** No porque ya no sirvan no quiere decir que los datos contenidos no estén expuestos a ser descubiertos.
***** En la delincuencia y robo de información las personas involucradas en actos ilícitos suelen reparar estos aparatos.
***** La información no se borra si solo se desecha el dispositivo, pues una memoria puede almacenar por cientos de años la información.
**** ¿Por qué se dice Oro en la basura electrónica?
***** Realmente los dispositivos se componen por partes de oro. 
***** Los circuitos se componen por metales preciosos como:
****** Oro
****** Plata
****** Cobre
****** Cobalto
***** La mayor parte de la basura electrónica no termina en fábricas de proceso de desechos electrónicos especializados. 
****** Los métodos para tratar esta basura suelen ser peligrosos para el medio ambiente.
***** Hoy en día es muy común que los delincuentes accedan a información personal de esta forma.
***** La basura electrónica se considera "Oro" ya que contiene información personal muy importante.
** **¿Cómo tratar la chatarra electrónica?**
*** Los aparatos electrónicos desechados se vuelven muy peligrosos y difíciles de tratar.
**** Actualmente son tantos los residuos electrónicos que ya son un gran problema ambiental.
*** Problema de desechos electrónicos.
**** Ante el constante cambio de aparatos debido a la vida moderna y a la moda.
***** Es muy usual el desecho de los teléfonos celulares, computadoras, etc.
**** El ministerio del ambiente estima que en Perú existe 400,000 mil toneladas de estos desechos.
*** RAEE
**** Se define como Residuos de Aparatos Eléctricos y Electrónicos
**** Muchas personas desconocen de este término y el peligro que esto conlleva. 
**** Son todos los aparatos electrónicos y eléctricos que tenemos en casa.
*** ¿Qué hacen las personas cuando no usan los aparatos?
**** Las regalan
**** Las tiran a la basura común.
**** Las guardan o coleccionan.



@endmindmap
```

